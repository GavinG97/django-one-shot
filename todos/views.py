from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)

    context = {"list_object": list}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=list)
    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def add_items(request):
    if request.method == "POST":
        items = TodoItemForm(request.POST)
        if items.is_valid():
            list = items.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        items = TodoItemForm()
    context = {
        "form": items,
    }
    return render(request, "todos/add_items.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "item_object": item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
